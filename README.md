Required packages:
- env
- discord.js
- request
- qs

Usage:
Bot has one command available

!meme TEMPLATE_ID "Top text" "Bottom text"

or

!meme help

Will send a direct message to the user with further instructions.

Configuration:
Create a `.env` file in the working directory (same as this file), and put the following configurations in it

TOKEN_DEV=<dev token>
TOKEN_PROD=<prod token>
BOTNAME=Kermit
IMGFLIP_ACC_USER=<img flip username>
IMGFLIP_ACC_PASS=<img flip password>