# Run this script to automatically respawn
# the process if it by any accident crashes.
# Be careful though this can cause infinite loops
# if the process exits at startup.
until npm start; do
    echo "Kermit crashed with exit code $?.  Respawning.." >&2
    sleep 1
done