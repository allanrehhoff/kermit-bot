module.exports = {
	sendHelp: function(msg, args) {
		let helpmsg = `Hey! I've heard you asked for help, so here it is:

Visit the following link to get the template ID's of the most popular memes:
http://development.rehhoff.me/memes.php

Usage: \`!meme <template_id> "Text 1" "Text 2" ...\` to build your meme. Including quotation marks.
You can use as many set of quotation marks as the meme supports text areas.

Tip: Pass an empty set of quotation marks to skip a text box.

Example: \`!meme 100777631 "" "Is this a meme?"\`\n`;

		msg.author.send(helpmsg);
	},
	serveMeme: function(msg, args) {
		var template_id = args[1];
		var qs = require("qs");
		var fetch = require("node-fetch");

		if(isNaN(template_id) || args[1] == '')  {
			msg.reply("\nI'm sorry, I did not understand your command.\nCorrect format: `!meme <template_id> \"Text 1\" \"Text 2\" ...`\nFor help type: `!meme help`");
			console.log("[USR] Invalid command submitted by " + msg.author.tag);
		} else {
			let apiArgs = {
				"template_id": template_id,
				"username": process.env.IMGFLIP_ACC_USER,
				"password": process.env.IMGFLIP_ACC_PASS,
				"boxes": []
			};

			let texts = args.slice(2).filter(() => {return true;});
			
			for(i = 0; i < texts.length; i++) {
				apiArgs.boxes.push({
					"text": texts[i]
				});
			}

			var strArgs = qs.stringify(apiArgs);
			var hrstart = process.hrtime();

			fetch("https://api.imgflip.com/caption_image?" + strArgs, {method: "POST"})
				.then(res => res.json())
				.then(json => {
					if(json.success == true) {
						msg.channel.send("", {file: json.data.url});

						hrend = process.hrtime(hrstart);
						console.log('[API] Meme delivery time (hr): %ds %dms', hrend[0], hrend[1] / 1000000);
					} else {
						msg.channel.send("That command is no good :slight_frown:\n" + json.error_message);

						throw new Error(json.error_message);
					}
				});
		}
	},
	execute: function(msg, args) {
		if(typeof args[0] !== "undefined" && args[0] == "!meme") {
			if(args[1] == "help") {
				this.sendHelp(msg, args);
			} else {
				this.serveMeme(msg, args);
			}
		}
	}
}