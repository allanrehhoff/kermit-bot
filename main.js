require("dotenv").config();

const Discord = require("discord.js");
const Command = require("./commands");

let bot = new Discord.Client;

if(process.argv.includes("--dev")) {
	var TOKEN = process.env.TOKEN_DEV;
} else {
	var TOKEN = process.env.TOKEN_PROD;
}

bot.login(TOKEN);

bot.on( "ready" , function() {
    console.log("[BOT] Logged in as: " + bot.user.tag);
});

bot.on("message", function(msg) {
	if(msg.author.username == process.env.BOTNAME) return;

	args = msg.content.match(/(?:[^\s"]+|"[^"]*")+/g);

	if(args != null && args.length > 0 && args[0].charAt(0) == "!") {
		var cmd = args[0].substr(1);

		if(Command.exists(cmd)) {
			// Remove wrapping quotation marks from args.
			// But preserve arg indexes
			for(index = 0; index < args.length; ++index) {
				args[index] = args[index].replace(/"/g, '');
			}

			try {
				Command.get(cmd).execute(msg, args);
			} catch (error) {
				console.error("[ERR] " + error);
				msg.channel.send('Ribbit! There was an error trying to execute that command.');
			}
		}
	}
});